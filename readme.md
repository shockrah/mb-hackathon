# Binary transmission format

This section outlines the structure of individual messages sent by each devices.

Data sent:

* Temperature
	* float
* Humidity Sensor
	* float : dword	: temp
	* float : dword : humidity
* Gas sensor data
	* long 	: dword : gas level

## Opcode table

|	Data Type	| 	Opcode	| 	arg0				|	arg1					|
|---			|---		|---					|---						|
|	Temperature	|	0xf1	|	dword(float/(temp))	|	none					|
|	Humidity	|	0xf2	| 	dword(float/(temp))	|	dword(float/(humidity))	|
|	Gas			|	0xf3	| 	dword(long)			|	none					|


## Englishified -> what now?

Arduino/Rpi will collect data about its surroundings:

* temp
* humidity
* flammable gas levels
* gps(_if we get a module_)

That data gets broadcast out somehow to a master server.
That server can then feed info to a site where we can decide how to respond.

## Broadcast data
Broadcast once every minute(or so).
Info be as follows:

```
f1 ?? ?? ?? ?? 
f2 ?? ?? ?? ?? | ?? ?? ?? ??
f3 ?? ?? ?? ??
f4 <nmea code maybe or not>
```

**NOTE:** We represent this line be line so that it's not cancerous to read but this is a regular binary format to follow. 

## Network stack

_I have no clue for this part but here are the options_:

1. Wifi
2. Sub-G Radio Frequency
3. GSM???
4. No idea

We'll use *something* and use that to send our data back to our master server, probably a raspberry pi, where we can host a dhcp listen server, each _client_ will just be the arduino's.
We then talk to the raspberry pi to find our what's going on with the arduino's.

*Redundancy cuz its 2am*

1. Turn on arduino with sensors and shit
2. Throw arduino(use a mortar cannon or smthng idk)
3. Turn on raspberry pi 
4. Talk to raspberry pi 
5. Win hackathon?
