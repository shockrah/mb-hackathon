#include <OneWire.h>
#include <dht.h>
#include <DallasTemperature.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP280.h>
#include <SPI.h>

/* humidity meme */
#define DHT11_PIN 7
dht DHT;
#define BMP_SCK 13
#define BMP_MISO 12
#define BMP_MOSI 11
#define BMP_CS 10

/* shit for the temp sensor */
#define ONE_WIRE_BUS 2
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);

/* some bmp thing */
Adafruit_BMP280 bme(BMP_CS, BMP_MOSI, BMP_MISO, BMP_SCK);

/* gas sensor threshhold */
int gasThresh = 400;
int smokeA0 = A5;

const int ledPin = 13; //pin 13 built-in led
const int soundPin = A0; //sound sensor attach to A0

void setup() {
  /* temp sensor */
  sensors.begin();

  /* gas sensors */
  pinMode(smokeA0, INPUT);

  if (!bme.begin()) {
    Serial.println("Could not find a valid BMP280 sensor, check wiring!");
  }
  pinMode(ledPin,OUTPUT);//set pin13 as OUTPUT
  Serial.begin(9600); //initialize serial
}

void loop() {
  /* TEMPERATURE SENSOR - PRIMARY */
  sensors.requestTemperatures();
  Serial.print("Primary Temp Senser: ");
  Serial.println(sensors.getTempCByIndex(0)); 

  /* next humidity pin read */
  int humdw = DHT.read11(DHT11_PIN);
  
  /* HUMIDITY SENSOR - Temperature */
  Serial.print("Temp 2: ");
  Serial.println(DHT.temperature);
  /* HUMIDITY SENSOR - Humidity */
  Serial.print("Humidity: ");
  Serial.println(DHT.humidity);

  /* GAS sensor shit */
  int gasAnalog = analogRead(A5);
  Serial.print("Flamaable Gas Coeff: ");
  Serial.println(gasAnalog);

  /* BAROMETER - Temperature */
  Serial.print("Temp 3:");
  Serial.print(bme.readTemperature());
  Serial.println(" *C");
    
  /* HBAROMETER - Air Pressure */
  Serial.print("Pressure: ");
  Serial.print(bme.readPressure());
  Serial.println(" Pa");

  /* BAROMETER - Altitude */
  Serial.print("Approx Altitude: ");
  Serial.print(bme.readAltitude(1013.25)); // this should be adjusted to your local forcase
  Serial.println(" m");

  /* getting sound levels */
  int value = analogRead(soundPin);//read the value of A0
  Serial.print("Sound level: ");
  Serial.println(value);//print the value
  if(value > 600) { //if the value is greater than 60
    digitalWrite(ledPin,HIGH);//turn on the led
    delay(200);	// sampling ever 200 ms
  }
  else {
    digitalWrite(ledPin,LOW); //turn off the led
  }
  Serial.println("====================");
  /* delay for lack of spam */
  delay(1000);
}
